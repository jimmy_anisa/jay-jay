import Home from '../views/Home';
import Group from '../views/Group'
import Project from '../views/Project'

export default [{
  name: 'Home',
  route: '/app',
  component: Home,
}, {
  name: 'Group',
  route: '/group',
  component: Group,
  // icon: GroupIcon
}, {
  name: 'Project',
  route: '/project',
  component: Project,
  // icon: ProjectIcon
}]