import React from "react";
import Card from "../components/Card";
import Progress from "../components/Progress";
import Icon from "../assets/images/Icons";
import Icons from "../assets/images/Icons";

const Home = () => (
  <div className="home">
    <div>
      <div className="col-md-8">
        <Card>
          <div className="heading">Current Module</div>
          <Card className="body">
            <div className="bookmark-container">
              <span className="bookmark">01</span>
              <span className="float-right">
                <Icons.Back fill="#FF3C3C" />
              </span>
            </div>
            <div className="heading">Introduction to Product Design/Figma</div>
          </Card>
          <Card className="body">
            <div className="timeline">
              <div>
              <Icon.Clipboard /> Design a landing page
              </div>
              <span className="float-right">
                <Icons.Back fill="#FF3C3C" />
              </span>
            </div>
          </Card>
        </Card>
      </div>
      <div className="col-md-4 text-center">
        <Card>
          <div className="heading">Course Progress</div>
          <div className="text-center">
            <Progress value={80} />
          </div>
          <div className="module-count">
            <p>26 Modules</p> <span className="circle" />
          </div>
        </Card>
      </div>
      <div className="col-md-12">
        <div className="heading modules">All Modules</div>
        {modules.map((module, index) => (
          <Card key={index}>
            <div className="content">
              <span className="title display-flex">
                <span className="count">{formatNumber(index + 1)}</span>
                {module}
              </span>
              <span className="float-right">
                {index === 0 ? <Icon.Back fill="#FF3C3C" /> : <Icon.Lock />}
              </span>
            </div>
          </Card>
        ))}
      </div>
    </div>
  </div>
);

const modules = [
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma",
  "Introduction to product design / figma"
];

const formatNumber = value => (value < 10 ? "0" + value : value);

export default Home;
