import React, { useState, useEffect } from "react";

const Progress = ({ value }) => {
  const [progressValue, setProgressValue] = useState(value);

  const animateValue = (start, end, duration) => {
    var range = end - start;
    var current = start;
    var increment = end > start ? 1 : -1;
    var stepTime = Math.abs(Math.floor(duration / range));
    var timer = setInterval(function() {
      current += increment;
      if (current === end) {
        clearInterval(timer);
      }
      setProgressValue(current);
    }, stepTime);
  };

  useEffect(() => {
    animateValue(0, value, 1000);
  }, [value]);

  return (
    <div className="progress">
      <div className={`pie-wrapper progress-${progressValue} style-2`}>
        <span className="label">
          {progressValue}
          <span className="smaller">%</span>
        </span>
        <div className="pie">
          <div className="left-side half-circle"></div>
          <div className="right-side half-circle"></div>
        </div>
        <div className="shadow"></div>
      </div>
    </div>
  );
};

export default Progress;
