import React from "react";
import routes from "../routes";
import Icon from '../assets/images/Icons'

const BreadCrumb = ({ location, toggleSidebar, sidebarOpen }) => {
  const getBrandText = () => {
    for (let i = 0; i < routes.length; i++) {
      if (
        location.pathname.indexOf(
          routes[i].route
        ) !== -1
      ) {
        return routes[i].name;
      }
    }
    return "Brand";
  };
  return (
    <div className="breadcrumb">
      <div className="hambuger" onClick={() => toggleSidebar(!sidebarOpen)} >
        <Icon.Project fill='#FF3C3C' />
        </div>
      <span>{getBrandText()}</span>
    </div>
  );
};

export default BreadCrumb;
