import React from 'react';
import { withRouter } from 'react-router-dom'

import AppLayout from './container/AppLayout';


const App = ({location}) => {
  return (
    <AppLayout location={location} />
  );
}

export default withRouter(App);
