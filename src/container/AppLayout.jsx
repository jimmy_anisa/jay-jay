import React, { useState } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import Sidebar from "../components/Sidebar";
import routes from "../routes";
import BreadCrumb from "../components/BreadCrumb";

const AppLayout = ({ location }) => {
  const [toggle, setToggle] = useState(false);
  return (
    <div className="app-layout">
      <Sidebar location={location} sidebarOpen={toggle} toggleSidebar={setToggle} />
      <div className={`overlay ${toggle ? 'open' : ''}`} onClick={() => setToggle(false)}></div>
      <div className="main">
        <BreadCrumb
          location={location}
          toggleSidebar={setToggle}
          sidebarOpen={toggle}
        />
        <Switch>
          {routes.map((route, index) => (
            <Route
              key={`${index}-${route.route}`}
              exact
              path={route.route}
              component={route.component}
            />
          ))}
          <Redirect to='/app' />
        </Switch>
      </div>
    </div>
  );
};

export default AppLayout;
